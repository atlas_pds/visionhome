package vh.server.application;

import java.sql.Connection;

import vh.server.cp.DataSource;

public class ServerAcceptor {

	public static Connection getConnection() {
		return DataSource.getConnection();
	}

	public static void closeConnections() {
		DataSource.closeConnections();		
	}

}
