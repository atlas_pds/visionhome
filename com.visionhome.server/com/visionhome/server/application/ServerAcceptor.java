package com.visionhome.server.application;

import java.sql.Connection;

import com.visionhome.server.connection.pool.DataSource;

public class ServerAcceptor {

	public static Connection getConnection() {
		return DataSource.getConnection();
	}

	public static void closeConnections() {
		DataSource.closeConnections();		
	}

}
